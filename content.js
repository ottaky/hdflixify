let thisURL = new URL(document.location.href)
if (thisURL.searchParams.has('e')) {
  let title = '????'
  let matches = document.location.href.match(/\.net\/(.+?)\//)
  if (matches) title = matches[1]
  fetch(`https://ajax.embed.is/heartbypass/get-source.php?eid=${thisURL.searchParams.get('e')}`)
  .then(response => { return response.json() })
  .then(data => {
    let ps = ''
    data.sources.forEach(source => {
      ps += `<p>curl --progress-bar -o ${title}.mp4 --insecure --referer '${document.location.href}' '${source.file}'</p>`
    })
    let iframe = document.createElement('iframe')
    iframe.setAttribute('width', '100%')
    iframe.setAttribute('height', '200px')
    iframe.style.frameBorder = '0'
    iframe.src = 'data:text/html,' + encodeURI(`<html><head><style>* { font-size: 12px; color: white; }</style><body>${ps}</body></html>`)
    let subHead = document.querySelector('#main > div > div.main-content.main-detail')
    subHead.insertBefore(iframe, subHead.childNodes[0])
  })
  .catch(err => {})
}
